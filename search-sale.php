<?php
//Inicia validación de credenciales.
if (!isset($_POST["objeto"])){
	header("Location: login.php");
	exit;
}

else{
	$objeto = json_decode($_POST["objeto"], true);
	$parametro = $objeto["fecha"];
	settype($parametro, 'string');

	// Establecer consulta
	$consulta = "
	SELECT DISTINCT id_transact, fecha
	FROM tmp_sale
	WHERE final_purchase = 1
	AND fecha LIKE '%".$parametro."%'
	";
	// Encapsular resultado
	include("Conexion.php");
	if ($resultado = mysqli_query($conexion, $consulta) or die ( "Ocurrio un error. Contacte al administrador del sistema")){
		if(mysqli_num_rows($resultado) > 0){
			$return = "";
			while($columna = mysqli_fetch_array($resultado)){
				$return.= "
				<tr>
				<td>".$columna['fecha']."</td>
				<td>".$columna['id_transact']."</td>
				<td><a href='viewPurchase.php?id=".$columna['id_transact']."' target='_blank'><img class='icon' src='img/search.png'></a></td>
				</tr>
				";
			}
			$return.= "</tbody></table>";
			echo json_encode($return);
		}
		else{
			echo json_encode(0);
		}
	}
	else {
		echo false;
	}
	mysqli_close($conexion);
}
?>
