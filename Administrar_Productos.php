<?php
session_start();
if(!isset($_SESSION['usuario']))
{
	header("Location: login.php");
	exit;
}
else
{
	?>
	<!-- Header -->
	<?php
	include('head.php');
	if(!$_GET){
		header('Location:Administrar_Productos.php?page=1');
	}
	?>
	<script src="js/functions.js"></script>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt_full">
		<div class="row">
			<div class="col-lg-12 text-left">
				<div class="form-inline">
					<div class="form-group mb-2" style="margin: 1% 1%;">
						<span><strong>Busqueda:</strong><img src="img/search.png" class="icon"></span>
					</div>
					<div class="form-group mx-sm-3 mb-2" style="margin: 0.5% 0%;">
						<input style="width: 500px;" type="text" class="form-control" id="search" placeholder="Ingresar un parámetro de busqueda..." onkeyup="searc()">
					</div>
					<div id="resultS" style="margin-left: 10px; margin-bottom: 2px;"></div>
				</div>
			</div>
			<div class="col-lg-12 text-center">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th scope="col">Código</th>
							<th scope="col">Proveedor</th>
							<th scope="col">Categoría</th>
							<th scope="col">Referencia</th>
							<th scope="col">Existencias</th>
							<th scope="col">Imagen</th>
							<th scope="col">Valor de Inversión</th>
							<th scope="col">Valor de Venta</th>
							<th scope="col">Descripción</th>
							<th scope="col">Acciones</th>
						</tr>
					</thead>
					<tbody id="printTable">
						<?php
						if (isset($_POST["objeto"])){

						}
						else {

							$consulta = "SELECT * FROM producto ORDER BY id ASC";
							include("Conexion.php");
							$resultado = mysqli_query($conexion, $consulta);
							$Tregistros = mysqli_num_rows($resultado);
							if(mysqli_num_rows($resultado) == 0){
								echo "<tr><td></td><td></td><td></td><td></td><td><p align='center'>No hay datos...</p></td><td></td><td></td><td></td><td></td><td></td></tr>";
							}
							else{
								$Article_x_Page = 10;
								$init = ($_GET['page']-1)*$Article_x_Page;
								$consulta2 = "SELECT * FROM producto ORDER BY id ASC LIMIT ".$init.", ".$Article_x_Page."";
								$result = mysqli_query($conexion, $consulta2);
								$table = "producto";
								$Tpages = ceil($Tregistros/$Article_x_Page);
								while ($columna = mysqli_fetch_array($result)){
									echo"
									<tr>
									<th scope='row'>".$columna['id']."</th>
									<td>".$columna['proveedor']."</td>
									<td>".$columna['tipo']."</td>
									<td>".$columna['nombre']."</td>
									";
									if ($columna['cantidad'] != 0){
										echo "
										<td>".$columna['cantidad']." Unidades</td>
										";
									} else{
										echo "
										<td style='color: #a94442 !important; font-size: 15px; background-color: #f2dede;'>Sin Existencias.</td>
										";
									}
									echo"
									<td><a href='images/".$columna['imagen']."' target='_blank'><img src='images/".$columna['imagen']."' class='product' /></a></td>
									<td>$ ".number_format($columna['valor_inversion'], 0, '', '.')."</td>
									<td>$ ".number_format($columna['valor_venta'], 0, '', '.')."</td>
									<td>".$columna['descripcion']."</td>
									<td><a href='#' data-toggle='modal' data-target='#UpdProduct' onclick='capdataupd(".chr(34).$columna['id'].chr(34).");'><img src='img/edit.png' class='icon' alt='Editar'></a> / <a href='#'  onclick='Delete(".chr(34).$columna["id"].chr(34).", ".chr(34).$table.chr(34).");'><img src='img/delete.png' class='icon' alt='Eliminar'></a></td>
									</tr>
									";
								}
							}
							mysqli_close($conexion);
							?>
						</tbody>
					</table>
				<?php } ?>
				<div class="container-fluid">

					<!-- Paginación -->
					<nav aria-label="Page navigation example" class="pagn">
						<ul class="pagination justify-content-center pag">
							<?php
							if($Tregistros > 10){
								$atribant = "";
								$atribsig = "";
								if($_GET['page'] == 1){
									$atribant = "disabled";
								}
								if ($_GET['page'] == $Tpages){
									$atribsig = "disabled";
								}
								echo "
								<li class='page-item ".$atribant."'><a class='page-link' href='Administrar_Productos.php?page=".($_GET['page']-1)."'>Anterior</a></li>
								";
								for($i = 1 ; $i <= $Tpages ; $i++) {
									$atribute = "";
									if ($_GET['page'] == $i){
										$atribute = "active";
									}
									echo "
									<li class='page-item ".$atribute."'><a class='page-link' href='Administrar_Productos.php?page=".$i."'>".$i."</a></li>
									";

								}
								echo "
								<li class='page-item ".$atribsig."'><a class='page-link' href='Administrar_Productos.php?page=".($_GET['page']+1)."'>Siguiente</a></li>
								";
							}
							?>
						</ul>
					</nav>
					<!-- /Paginación -->

				</div>
			</div>
		</div>


		<!-- Formulario de edición -->
		<div id="UpdProduct" class="modal fade" role="dialog">
			<div class="modal-dialog" style="max-width: 1060px !important;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
						<label style="color: #FFF;">Editar Producto</label>
						<button type="button" class="close" data-dismiss="modal" onclick="reload();" style="color: #FFF;">&times;</button>
					</div>
					<div style="width: 100%; padding:1%;" id="product">
						<form id="form_uprod" action="proc_updproduct.php" method="POST" enctype="multipart/form-data">
							<div class="col-lg-6" style="float:left;">
								<div class="form-group">
									<input type="text" id="idp" name="idp" style="display: none;">
									<input id="referencia" name="referencia" type="hidden" value="1">
									<label style="text-align: left; display: block; margin: -1% 0% !important;">Seleccione el proveedor<label class="rqrd">*</label></label>
									<select class="custom-select" id="proveedor" name="proveedor">
										<option selected>Seleccione</option>
										<?php
										include("Conexion.php");
										$consulta = "SELECT * FROM proveedor";
										$resultado = mysqli_query($conexion, $consulta);
										while ($columna = mysqli_fetch_array($resultado)){
											echo"
											<option value='".$columna["nombre_proveedor"]."'>".$columna["nombre_proveedor"]."</option>
											";
										}
										mysqli_close($conexion);
										?>
									</select>
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -1% 0% !important;">Seleccione la categoria<label class="rqrd">*</label></label>
									<select class="custom-select" id="tipo" name="tipo">
										<option selected>Seleccione</option>
										<?php
										include("Conexion.php");
										$consulta = "SELECT * FROM categoria";
										$resultado = mysqli_query($conexion, $consulta);
										while ($columna = mysqli_fetch_array($resultado)){
											echo"
											<option value='".$columna["categoria"]."'>".$columna["categoria"]."</option>
											";
										}
										mysqli_close($conexion);
										?>
									</select>
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -2% 0% !important;">Referencia del producto<label class="rqrd">*</label></label>
									<input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Nombre del producto">
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -1% 0% !important;">Ingrese las unidades disponibles<label class="rqrd">*</label></label>
									<input type="number" min="0" class="form-control" id="cantidad" name="cantidad" placeholder="0">
								</div>
							</div>
							<div class="col-lg-6" style="float:left;">
								<div class="form-group">
									<label style="text-align: left; display: block; margin: 1% 0% !important;">Imagen</label>
									<img class="iconForm" id="iconForm"></img>
									<input class="form-control" maxlength="150" type="file" name="imagen" id="imagen" data-validation-allowing="jpg, png, gif">
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -2% 0% !important;">Valor de inversión<label class="rqrd">*</label></label>
									<input type="text" class="form-control" id="valorI" name="valorI" placeholder="$0.00">
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -2% 0% !important;">Valor de venta<label class="rqrd">*</label></label>
									<input type="text" class="form-control" id="valorV" name="valorV" placeholder="$0.00">
								</div>
							</div>
							<div class="col-lg-12" style="float:left;">
								<div class="form-group">
									<label style="text-align: left; display: block; margin: 0% 0% !important;">Descripción del producto</label>
									<textarea type="text" class="form-control" id="descripcion" name="descripcion"></textarea>
								</div>
								<button type="button" class="btn btn-primary" onclick="Product('uproduct');">Editar</button>
							</div>
						</form>
					</div>
					<div class="col-lg-12" style="float:left; margin-bottom: 1%;"><div id="resultprdct"></div></div>
				</div>
			</div>
		</div>

		<!-- /Formulario de edición -->

	</div>
	<!-- /Page Content -->

	<!-- Footer -->
	<?php
	include('footer.php');
	?>
	<!-- /Footer -->
	<?php
}
?>
