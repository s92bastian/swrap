<?php
//Inicia validación de credenciales.
session_start();
if(!isset($_SESSION['usuario'])){
	header("Location: login.php");
	exit;
} else{
	date_default_timezone_set('America/Bogota');
	require('fpdf/fpdf.php');

	/*
	* Generate purchase From FPDF
	*/
	$id_transact = $_POST["id_transact"];
	// Establecer consultas
	$consult = "
	SELECT cedula, nombre_c
	FROM tmp_sale
	WHERE id_transact LIKE '".$id_transact."'
	";
	$consult1 = "
	SELECT pr.nombre as nombre, pr.descripcion as descripcion, tm.unidades as unidades, tm.preciof as preciof, tm.cedula as cedula, tm.nombre_c as nombre_c, tm.id_producto as id_producto
	FROM producto pr
	JOIN tmp_sale tm
	ON tm.id_producto = pr.id
	AND id_transact LIKE '".$id_transact."'
	";
	$consult2 = "
	SELECT * FROM empresa
	";
	$consult3 = "
	SELECT MAX(consecutivoFactura) FROM tmp_sale
	";
	// Encapsular resultado
	include("Conexion.php");
	$result  = mysqli_query($conexion, $consult) or die ("Ocurrio un error recopilando la información de facturación. Contacte al administrador del sistema");
	$result1 = mysqli_query($conexion, $consult1) or die ("Ocurrio un error recopilando la información de facturación. Contacte al administrador del sistema");
	$result2 = mysqli_query($conexion, $consult2) or die ("Ocurrio un error recopilando la información de facturación. Contacte al administrador del sistema");
	$result3 = mysqli_query($conexion, $consult3) or die ("Ocurrio un error recopilando la información de facturación. Contacte al administrador del sistema");

	if(mysqli_num_rows($result1) > 0 && mysqli_num_rows($result2) > 0){

		$row  = mysqli_fetch_array($result);
		$row1 = mysqli_fetch_all($result1, MYSQLI_ASSOC);
		$row2 = mysqli_fetch_array($result2);
		$row3 = mysqli_fetch_array($result3);
		//Si ya hay registradas facturas finales, entonces..
		if($row3[0] > 0){
			$consecutivo = ($row3[0] + 1);
			$cu = strlen($consecutivo);
			//Si hay ceros al lado izquierdo, entonces...
			if($cu < 8){
				$r = 8 - $cu;
				//Poner ceros a la izquierda perdidos al convertir cadena a entero.
				$zero = "";
				for($i = 1; $i <= $r; $i++){
					$zero = "0".$zero;
				}
				$consecutivo = $zero."".$consecutivo;
			}
		} else{
			$cu = strlen($row2["consecutivoFactura"]);
			//Si hay ceros al lado izquierdo, entonces...
			if($cu < 8){
				$r = 8 - $cu;
				//Poner ceros a la izquierda perdidos al convertir cadena a entero.
				$zero = "";
				for($i = 1; $i <= $r; $i++){
					$zero = "0".$zero;
				}
				$consecutivo = $zero."".$row2["consecutivoFactura"];
			}
		}

		/*
		*Pdf config and settings
		*/

		$pdf = new FPDF('P', 'mm', 'A4');
		$pdf->SetMargins(20, 20, 15);
		$pdf->SetFont('Arial', 'B', 12);

		/*
		*	Se crea nueva página solo si se han realizado los cambios sobre la BD exitosamente.
		*/

		//Registro final de venta.
		$status = 1;
		$a = "
		UPDATE tmp_sale
		SET final_purchase = true,
		consecutivoFactura = ".$consecutivo."
		WHERE id_transact = ".$id_transact."
		";
		$b  = mysqli_query($conexion, $a) or die ("Ocurrio un error registrando la factura final en la base de datos. Contacte al administrador del sistema");

		//Si se registro exitosamente la factura como "Factura final", entonces...
		if (mysqli_affected_rows($conexion) > 0){

			$pdf->AddPage();
			$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
			$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			$dateNow = $dias[date("w")]." ".date("d")." de ".$meses[date("n") - 1]." del ".date("Y");

			//Si hay logo entonces..
			if ($row2["logo"] != ""){
				//Logo
				$pdf->Image("images/".$row2["logo"],20,20,33);
				//Datos de la empresa
				$pdf->SetFont('Arial','',12);
				$pdf->Cell(75, 15);
				$pdf->Cell(30,6,utf8_decode($row2["empresa"]),0,1,'C');
				//Si hay NIT entonces..
				if($row2["nit"] != ""){
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("NIT: ".$row2["nit"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Teléfono: ".$row2["telefono"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Dirección: ".$row2["direccion"]),0,1,'C');
				} else{
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Teléfono: ".$row2["telefono"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Dirección: ".$row2["direccion"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode(""),0,1,'C');
				}

				//QR
				$pdf->Image("images/qr.png",168, 18, 35);

			} else{
				//Datos de la empresa
				$pdf->SetFont('Arial','',12);
				$pdf->Cell(75, 15);
				$pdf->Cell(30,6,utf8_decode($row2["empresa"]),0,1,'C');
				//Si hay NIT entonces..
				if($row2["nit"] != ""){
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("NIT: ".$row2["nit"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Teléfono: ".$row2["telefono"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Dirección: ".$row2["direccion"]),0,1,'C');
				} else{
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Teléfono: ".$row2["telefono"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode("Dirección: ".$row2["direccion"]),0,1,'C');
					$pdf->Cell(75, 15);
					$pdf->Cell(30,6,utf8_decode(""),0,1,'C');
				}
				//QR
				$pdf->Image("images/qr.png",168, 18, 35);
			}
			//Fecha y No de factura.
			$pdf->SetFont('Arial','B',9);
			$pdf->ln(15);
			$pdf->Cell(90, 10, utf8_decode("Fecha de factura: ".$dateNow), 1, 0, 'L');
			$pdf->Cell(90, 10, utf8_decode("Factura No. ".$consecutivo), 1, 1, 'R');

			// Datos del cliente
			$pdf->ln(2);
			$pdf->SetFont('Arial','B',12);
			$pdf->Cell(90, 8, utf8_decode("Información del cliente:"), 0, 1, 'L');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(90, 5, utf8_decode("Nombre: ".$row["nombre_c"]), 0, 1, 'L');
			$pdf->Cell(90, 5, utf8_decode("Cédula: ".$row["cedula"]), 0, 1, 'L');

			// Detalle de facturación
			$pdf->ln(2);
			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(180, 10, utf8_decode("Detalle de Factura"), 1, 1, 'C');

			//Comienza printeo
			// Column widths
			$w = array(35, 95, 25, 25);
			// Cabecera de tabla
			$header = array(utf8_decode("Referencia"), utf8_decode("Descripción"), utf8_decode("Cantidad"), utf8_decode("Valor"));
			for($i=0;$i<count($header);$i++)
			$pdf->Cell($w[$i],7,$header[$i],1,0,'C');
			$pdf->Ln();

			// Printiando datos
			$pdf->SetFont('Arial','',10);
			$y  = 108;
			$t = 0;
			foreach ($row1 as $row){

				//Descontar cantidad de productos vendidos de las existencias actuales.
				$c = "
				UPDATE producto
				SET cantidad = (cantidad - ".$row["unidades"].")
				WHERE id = ".$row["id_producto"]."
				";
				echo $c;
				$d  = mysqli_query($conexion, $c) or die ("Ocurrio un error descontando las unidades vendidas en la base de datos. Contacte al administrador del sistema");

				//Inicia tabla de datos
				$pdf->MultiCell($w[0], 10, $row["nombre"], 1, 'LTR', 0);
				$pdf->SetXY(55,$y);
				$pdf->MultiCell($w[1], 10, $row["descripcion"], 1, 'LTR', 0);
				$pdf->SetXY(150,$y);
				$pdf->Cell($w[2],10,number_format($row["unidades"]),1,0,'R');
				$pdf->Cell($w[3],10,number_format($row["preciof"]),1,0,'R');
				$pdf->Ln();
				$y = ($y+10);
				$t+=$row["preciof"];
			}
			// Cerrando tabla con total
			$pdf->Cell(array_sum($w),0,'','T');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
			$pdf->Cell(180, 10, "TOTAL: $".number_format($t), 1, 1, 'R');
			//Finaliza printeo

			//Pie de factura.
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(90, 8, utf8_decode("OBSERVACIONES:"), 0, 1, 'L');
			$pdf->SetFont('Arial','',8);
			$pdf->MultiCell(180, 5, utf8_decode($row2["pieFactura"]), 0, 'FJ');
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(180, 10, utf8_decode("Conserve esta factura como soporte para efectos de garantía."), 0, 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(180, 10, $row2["empresa"]." - ".date("Y")." Todos los derechos reservados.", 0, 1, 'C');

			ob_end_clean();
			//Almacenamiento del recibo de factura en ruta local
			$pdf->Output('F', 'Purchase-record\Purchase-'.$id_transact.'.pdf');

			//Mostrar factura en el navegador
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="Purchase-'.$id_transact.'"');
			readfile("Purchase-record\Purchase-".$id_transact.".pdf");

		} else{
			echo "Error grave!, Contacte al administrador del sistema.";
		}


	} else{
		echo false;
	}
}
