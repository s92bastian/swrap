<?php
//Inicia validación de credenciales.
if (!isset($_POST["objeto"])){
	header("Location: login.php");
	exit;
}

else{
	$objeto = json_decode($_POST["objeto"], true);
	$parametro = $objeto["search"];
	settype($parametro, 'string');
	$table = "cliente";
	// Establecer consulta
	$consulta = "
	SELECT * FROM ".$table."
	WHERE nombre LIKE '%".$parametro."%'
	OR apellidos LIKE '%".$parametro."%'
	OR cedula LIKE '%".$parametro."%'
	OR celular LIKE '%".$parametro."%'
	OR correo LIKE '%".$parametro."%'
	";
	// Encapsular resultado
	include("Conexion.php");
	if ($resultado = mysqli_query($conexion, $consulta) or die ( "Ocurrio un error. Contacte al administrador del sistema")){
		if(mysqli_num_rows($resultado) > 0){
			$return = "";
			while($columna = mysqli_fetch_array($resultado)){
				$return.= "
				<tr>
				<th scope='row'>".$columna['nombre']."</th>
				<td>".$columna['apellidos']."</td>
				<td>".$columna['cedula']."</td>
				<td>".$columna['celular']."</td>
				<td>".$columna['correo']."</td>
				<td><a href='#' data-toggle='modal' data-target='#UpdProduct' onclick='capdataucl(".chr(34).$columna['id'].chr(34).");'><img src='img/edit.png' class='icon' alt='Editar'></a> / <a href='#' onclick='Delete(".chr(34).$columna["id"].chr(34).", ".chr(34).$table.chr(34).");'><img src='img/delete.png' class='icon' alt='Eliminar'></a></td>
				</tr>
				";

			}
			$return.= "</tbody></table>";
			echo json_encode($return);
		}
		else{
			echo json_encode(0);
		}
	}
	else {
		echo false;
	}
	mysqli_close($conexion);
}
?>
