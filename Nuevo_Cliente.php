<?php
session_start();
if(!isset($_SESSION['usuario']))
{
	header("Location: login.php");
	exit;
}
else
{
	?>
	<!-- Header -->
	<?php
	include('head.php');
	?>
	<script src="js/functions.js"></script>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="form_amt" id="client">
					<strong>Información del Cliente</strong>
					<div id="resultnvclnt" style="margin-top: 8px;"></div>
					<input id="PF" name="PF" type="hidden" value="A">
					<input type="text" id="idc" style="display: none;">
					<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					<div class="form-group">
						<input id="referenciaCliente" name="referenciaCliente" type="hidden" value="1">
						<label style="text-align: left; display: block; margin: 0% 0% !important;">Nombres<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="nombre" aria-describedby="emailHelp">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: -0% 0% !important;">Apellidos<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="apellidos" aria-describedby="emailHelp">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block;">Número de cédula</label>
						<input type="text" class="form-control" id="cedulaM" aria-describedby="emailHelp" onkeyup="ced()" onchange="ced()">
						<div id="resultced" style="margin-bottom: 1%;"></div>
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: -0% 0% !important;">Celular<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="celular" aria-describedby="emailHelp">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block;">Correo electrónico<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="correo" aria-describedby="emailHelp">
					</div>
					<button id="updpr" type="submit" class="btn btn-primary" onclick="SavePerson();">Guardar</button>

				</div>
			</div>
		</div>
	</div>
	<!-- /Page Content -->

	<!-- Footer -->
	<?php
	include('footer.php');
	?>
	<!-- /Footer -->
	<?php
}
?>
