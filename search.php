<?php
//Inicia validación de credenciales.
if (!isset($_POST["objeto"])){
	header("Location: login.php");
	exit;
}

else{
	$objeto = json_decode($_POST["objeto"], true);
	$parametro = $objeto["search"];
	settype($parametro, 'string');

	// Establecer consulta
	$consulta = "
	SELECT * FROM producto
	WHERE tipo LIKE '%".$parametro."%'
	OR nombre LIKE '%".$parametro."%'
	OR descripcion LIKE '%".$parametro."%'
	";
	// Encapsular resultado
	include("Conexion.php");
	if ($resultado = mysqli_query($conexion, $consulta) or die ( "Ocurrio un error. Contacte al administrador del sistema")){
		if(mysqli_num_rows($resultado) > 0){
			$return = "";
			while($columna = mysqli_fetch_array($resultado)){
				$return.= "<tr><th scope='row'>".$columna['id']."</th><td>".$columna['proveedor']."</td><td>".$columna['tipo']."</td><td>".$columna['nombre']."</td>";
				if ($columna['cantidad'] != 0){
					echo "
					<td>".$columna['cantidad']." Unidades</td>
					";
				} else{
					echo "
					<td style='color: #a94442 !important; font-size: 15px; background-color: #f2dede;'>Sin Existencias.</td>
					";
				}
				echo" Unidades</td><td><a href='".$columna['imagen']."' target='_blank'><img src='images/".$columna['imagen']."' class='product' /></a></td><td>$ ".number_format($columna['valor_inversion'], 0, '', '.')."</td><td>$ ".number_format($columna['valor_venta'], 0, '', '.')."</td><td>".$columna['descripcion']."</td><td><a href='#' data-toggle='modal' data-target='#UpdProduct' onclick='capdataupd(".chr(34).$columna['id'].chr(34).");'><img src='img/edit.png' class='icon' alt='Editar'></a> / <a href='#'  onclick='Delete(".chr(34).$columna["id"].chr(34).", 'producto');'><img src='img/delete.png' class='icon' alr='Eliminar'></a></td></tr>";
			}
			$return.= "</tbody></table>";
			echo json_encode($return);
		}
		else{
			echo json_encode(0);
		}
	}
	else {
		echo false;
	}
	mysqli_close($conexion);
}
?>
