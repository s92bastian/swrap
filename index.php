<?php
	session_start();
  	if(!isset($_SESSION['usuario']))
	{
    header("Location: login.php");
    exit;
	}
  else
  {
?>
		<!-- Header -->
		<?php
		include('head.php');
		?>
		<!-- Header -->

		<!-- Menu -->
		<?php
		include('menu.php');
		?>
		<!-- /Menu -->

		<!-- Page Content -->
			<div class="index">
				<?php
				$consulta1 = "SELECT * FROM categoria";
				$consulta2 = "SELECT * FROM producto";
				$consulta3 = "SELECT SUM(preciof) FROM tmp_sale";
				include("Conexion.php");
				$result1 = mysqli_query($conexion, $consulta1) or die ( "Ocurrio un error. Contacte al administrador del sistema");
				$result2 = mysqli_query($conexion, $consulta2) or die ( "Ocurrio un error. Contacte al administrador del sistema");
				$result3 = mysqli_query($conexion, $consulta3) or die ( "Ocurrio un error. Contacte al administrador del sistema");
				$value1 = mysqli_num_rows($result1);
				$value2 = mysqli_num_rows($result2);
				$value3 = mysqli_fetch_row($result3);
				mysqli_close($conexion);
				?>
				<div class="row">
					<div class="col-md-3">
					   <div class="panel panel-box clearfix">
						 <div class="panel-icon pull-leftt bg-red">
						  <img src="img/categ.png" style="width: 100%;">
						</div>
						<div class="panel-value pull-right">
						  <h2 class="margin-top"> <?php  echo $value1; ?> </h2>
						  <p class="text-muted">Categorías</p>
						</div>
					   </div>
					</div>
					<div class="col-md-1">
					</div>
					<div class="col-md-3">
					   <div class="panel panel-box clearfix">
						 <div class="panel-icon pull-leftt bg-blue">
						  <img src="img/product.png" style="width: 100%;">
						</div>
						<div class="panel-value pull-right">
						  <h2 class="margin-top"> <?php  echo $value2; ?> </h2>
						  <p class="text-muted">Productos</p>
						</div>
					   </div>
					</div>
					<div class="col-md-1">
					</div>
					<div class="col-md-3">
					   <div class="panel panel-box clearfix">
						 <div class="panel-icon pull-leftt bg-yellow">
						  <img src="img/sales.png" style="width: 100%;">
						</div>
						<div class="panel-value pull-right">
						  <h2 class="margin-top" style="font-size: 25px;"><?php  echo number_format($value3[0]); ?></h2>
						  <p class="text-muted">Ventas</p>
						</div>
					   </div>
					</div>
</div>
			</div>
		<!-- /Page Content -->

		<!-- Footer -->
		<?php
		include('footer.php');
		?>
		<!-- /Footer -->
<?php
  }
?>
