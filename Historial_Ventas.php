<?php
session_start();
if(!isset($_SESSION['usuario']))
{
	header("Location: login.php");
	exit;
}
else
{
	?>
	<!-- Header -->
	<?php
	include('head.php');
	if(!$_GET){
		header('Location:Historial_Ventas.php?page=1');
	}
	?>
	<script src="js/functions.js"></script>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt">
		<div class="row">
			<div class="col-lg-12 text-left">
				<div class="form-inline">
					<div class="form-group mb-2" style="margin: 1% 1%;">
						<span><strong>Busqueda:</strong><img src="img/search.png" class="icon"></span>
					</div>
					<div class="form-group mx-sm-3 mb-2" style="margin: 0.5% 0%;">
						<input style="width: 500px;" type="text" class="form-control" id="fecha" placeholder="Fecha como parámetro de busqueda" onkeyup="searcSale()">
					</div>
					<div id="resultSale" style="margin-left: 10px; margin-bottom: 2px;"></div>
				</div>
			</div>
			<div class="col-lg-12 text-center">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
              <th scope="col">Fecha</th>
              <th scope="col">Recibo de factura #</th>
              <th scope="col">Acciones</th>
            </tr>
					</thead>
					<tbody id="printSale">
						<?php
						if (isset($_POST["objeto"])){

						}
						else {

							$consulta = "
	            SELECT DISTINCT id_transact, fecha
	            FROM tmp_sale
	            WHERE final_purchase = 1
	            ";
							include("Conexion.php");
							$resultado = mysqli_query($conexion, $consulta);
							$Tregistros = mysqli_num_rows($resultado);
							if(mysqli_num_rows($resultado) == 0){
								echo "<tr><td></td><td><p align='center'>No hay datos...</p></td><td></td></tr>";
							}
							else{
								$Article_x_Page = 10;
								$init = ($_GET['page']-1)*$Article_x_Page;
								$consulta2 = "
								SELECT DISTINCT id_transact, fecha
		            FROM tmp_sale
		            WHERE final_purchase = 1
								ORDER BY id_transact ASC LIMIT ".$init.", ".$Article_x_Page."
								";
								$result = mysqli_query($conexion, $consulta2);
								$Tpages = ceil($Tregistros/$Article_x_Page);
								while ($columna = mysqli_fetch_array($result)){
									echo"
	                <tr>
	                <td>".$columna['fecha']."</td>
	                <td>".$columna['id_transact']."</td>
	                <td><a href='viewPurchase.php?id=".$columna['id_transact']."' target='_blank'><img class='icon' src='img/search.png'></a></td>
	                </tr>
	                ";
								}
							}
							mysqli_close($conexion);
							?>
						</tbody>
					</table>
				<?php } ?>
				<div class="container-fluid">

					<!-- Paginación -->
					<nav aria-label="Page navigation example" class="pagn">
						<ul class="pagination justify-content-center pag">
							<?php
							if($Tregistros > 10){
								$atribant = "";
								$atribsig = "";
								if($_GET['page'] == 1){
									$atribant = "disabled";
								}
								if ($_GET['page'] == $Tpages){
									$atribsig = "disabled";
								}
								echo "
								<li class='page-item ".$atribant."'><a class='page-link' href='Historial_Ventas.php?page=".($_GET['page']-1)."'>Anterior</a></li>
								";
								for($i = 1 ; $i <= $Tpages ; $i++) {
									$atribute = "";
									if ($_GET['page'] == $i){
										$atribute = "active";
									}
									echo "
									<li class='page-item ".$atribute."'><a class='page-link' href='Historial_Ventas.php?page=".$i."'>".$i."</a></li>
									";

								}
								echo "
								<li class='page-item ".$atribsig."'><a class='page-link' href='Historial_Ventas.php?page=".($_GET['page']+1)."'>Siguiente</a></li>
								";
							}
							?>
						</ul>
					</nav>
					<!-- /Paginación -->

				</div>
			</div>
		</div>

	</div>
	<!-- /Page Content -->

	<!-- Footer -->
	<?php
	include('footer.php');
	?>
	<!-- /Footer -->
	<?php
}
?>
