<?php
//Inicia validación de credenciales.
session_start();
if(!isset($_SESSION['usuario'])) {
	header("Location: login.php");
	exit;
} else{

  $id = $_GET['id'];
  header('Content-type: application/pdf');
  header('Content-Disposition: inline; filename="Purchase-'.$id.'"');
  readfile("Purchase-record\Purchase-".$id.".pdf");

}
?>
