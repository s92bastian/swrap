<?php
	session_start();
  	if(!isset($_SESSION['usuario']))
	{
    header("Location: login.php");
    exit;
	}
  else
  {
?>
		<!-- Header -->
		<?php
		include('head.php');
		?>
		<script src="js/functions.js"></script>
		<!-- Header -->

		<!-- Menu -->
		<?php
		include('menu.php');
		?>
		<!-- /Menu -->

		<!-- Page Content -->
			<div class="containeramt_full">
				<div class="row">
					<div class="col-lg-12 text-center">
						<table class="table table-striped table-bordered"  style="text-align: left !important;">
						  <thead>
							<tr>
							  <th scope="col">Código</th>
							  <th scope="col">Proveedor</th>
							  <th scope="col">Contácto</th>
							  <th scope="col">Teléfono</th>
							  <th scope="col">Celular</th>
							  <th scope="col">Correo</th>
							  <th scope="col">Dirección</th>
							  <th scope="col">Acciones</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							include("Conexion.php");
							$consulta = "SELECT *
										 FROM proveedor
										 ";
								$table = "proveedor";
								$resultado = mysqli_query($conexion, $consulta);
								if(mysqli_num_rows($resultado) == 0){
									echo "<tr><td></td><td></td><td></td><td><p align='center'>No hay datos...</p></td><td></td><td></td><td></td><td></td></tr>";
								}
								else{
									while ($columna = mysqli_fetch_array($resultado)){
										echo"
											<tr>
											  <td><strong>".$columna['id']."</strong></td>
											  <td>".$columna['nombre_proveedor']."</td>
											  <td><a href='mailto:".$columna["correo"]."?subject=Contácto%20generado%20automáticamente%20por%20SWRAP'><img src='img/mail.png' class='icon' style='margin-bottom: 2px;'></a><a target='_blank' href='https://web.whatsapp.com/send?phone=57".$columna["celular"]."&text=Cordial%20saludo.'><img src='img/wp.png' style='width: 27px;'></a>".$columna['nombre_contacto']."</td>
											  <td>".$columna['telefono']."</td>
											  <td>".$columna['celular']."</td>
											  <td>".$columna['correo']."</td>
											  <td>".$columna['direccion']."</td>
											  <td><a href='#' data-toggle='modal' data-target='#UpdProv' onclick='capdataupdprov(".chr(34).$columna['id'].chr(34).");'><img src='img/edit.png' class='icon' alt='Editar'></a> / <a href='#'  onclick='Delete(".chr(34).$columna["id"].chr(34).", ".chr(34).$table.chr(34).");'><img src='img/delete.png' class='icon' alt='Eliminar'></a></td>
											</tr>
										";
									}
								}
								mysqli_close($conexion);
							?>
						  </tbody>
						</table>
					</div>
				</div>


				<!-- Formulario de edición -->
				<div id="UpdProv" class="modal fade" role="dialog">
				  <div class="modal-dialog">
			   <!-- Modal content-->
					   <div class="modal-content">
							<div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
								<label style="color: #FFF;">Editar Categoría</label>
								<button type="button" class="close" data-dismiss="modal" onclick="reload();" style="color: #FFF;">&times;</button>
							</div>
							<div class="form_amt" id="product">
							<input type="text" id="id" style="display: none;">
							<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
								<div class="form-group">
								<label style="text-align: left; display: block; margin: -1% 0% !important;">Nombre del proveedor<label class="rqrd">*</label></label>
								<input type="text" class="form-control" id="nombrep" name="nombrep" placeholder="Ej: Samsung">
							  </div>
							  <div class="form-group">
								<label style="text-align: left; display: block; margin: -1% 0% !important;">Nombre del contacto<label class="rqrd">*</label></label>
								<input type="text" class="form-control" id="nombrec" placeholder="Jhon..">
							  </div>
							  <div class="form-group">
								<label style="text-align: left; display: block; margin: 0.2% 0% !important;">Teléfono de contácto</label>
								<input type="text" class="form-control" id="telefono" placeholder="+(57)">
							  </div>
							  <div class="form-group">
								<label style="text-align: left; display: block; margin: -1% 0% !important;">Celular de contácto<label class="rqrd">*</label></label>
								<input type="text" class="form-control" id="celular" placeholder="+(57)">
							  </div>
							  <div class="form-group">
								<label style="text-align: left; display: block; margin: 0.2% 0% !important;">Correo electrónico</label>
								<input type="email" class="form-control" id="correo" placeholder="correo@electronico.com">
							  </div>
							  <div class="form-group">
								<label style="text-align: left; display: block; margin: 0.2% 0% !important;">Dirección del proveedor</label>
								<input type="text" class="form-control" id="direccion" placeholder="Dirección">
							  </div>
								<button id="updpr" type="submit" class="btn btn-primary" onclick="UpdateProv();">Editar</button>
								<div id="resultupdtprv" style="margin-top:1%;"></div>
						</div>
					   </div>
				  </div>
				</div>

				<!-- /Formulario de edición -->

			</div>
		<!-- /Page Content -->

		<!-- Footer -->
		<?php
		include('footer.php');
		?>
		<!-- /Footer -->
<?php
  }
?>
