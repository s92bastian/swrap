<?php
//Inicia validación de credenciales.
if (!isset($_POST["objeto"])){
	header("Location: login.php");
	exit;
}

else{

	$objeto = json_decode($_POST["objeto"], true);

	if(isset($objeto["status"])){

		$tabla = $objeto["tabla"];

		// Establecer consulta
		$consulta = "SELECT * FROM ".$tabla.";";
		// Encapsular resultado
		include("Conexion.php");
		if ($resultado = mysqli_query($conexion, $consulta) or die ( "Ocurrio un error. Contacte al administrador del sistema")){
			if(mysqli_num_rows($resultado) > 0){
				$columna = mysqli_fetch_array($resultado);
				echo json_encode($columna);
			} else {
				echo false;
			}
		}
		mysqli_close($conexion);

	} else{

		$id = $objeto["id"];
		$tabla = $objeto["tabla"];

		// Establecer consulta
		$consulta = "SELECT * FROM ".$tabla." WHERE id = ".$id.";";
		// Encapsular resultado
		include("Conexion.php");
		if ($resultado = mysqli_query($conexion, $consulta) or die ( "Ocurrio un error. Contacte al administrador del sistema.")){
			$columna = mysqli_fetch_array($resultado);
			echo json_encode($columna);
		}
		else {
			echo false;
		}
		mysqli_close($conexion);
	}
}
?>
