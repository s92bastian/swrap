<?php
session_start();
if(!isset($_SESSION['usuario']))
{
	header("Location: login.php");
	exit;
}
else
{
	?>
	<!-- Header -->
	<?php
	include('head.php');
	?>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt">
		<div class="row">
			<div class="form_amt" id="person">
				<form id="form_prod" action="proc_prod.php" method="post" enctype="multipart/form-data">
					<p align="center"><strong>Información del Producto</strong></p>
					<div class="col-lg-12" style="float:left;">
						<div id="resultprdct" style="margin-bottom: 1%;"></div>
					</div>
					<div class="col-lg-6" style="float:left;">
						<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
						<div class="form-group">
							<input id="referencia" name="referencia" type="hidden" value="1">
							<label style="text-align: left; display: block; margin: -1% 0% !important;">Seleccione el proveedor<label class="rqrd">*</label></label>
							<?php
							include("Conexion.php");
							$consulta = "SELECT * FROM proveedor";
							$resultado = mysqli_query($conexion, $consulta);
							if(mysqli_num_rows($resultado) == 0){
								echo"
								<select class='custom-select' id='proveedor' disabled>
								<option selected>No hay proveedores en el sistema aún.</option>
								</select>
								";
							}
							else{
								echo "<select class='custom-select' id='proveedor' name='proveedor'>
								<option selected>Seleccione</option>
								";
								while ($columna = mysqli_fetch_array($resultado)){
									echo"
									<option value='".$columna["nombre_proveedor"]."'>".$columna["nombre_proveedor"]."</option>
									";
								}
								echo"</select>";
							}
							mysqli_close($conexion);
							?>
						</select>
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: -1% 0% !important;">Seleccione la categoria<label class="rqrd">*</label></label>
						<?php
						include("Conexion.php");
						$consulta = "SELECT * FROM categoria";
						$resultado = mysqli_query($conexion, $consulta);
						if(mysqli_num_rows($resultado) == 0){
							echo"
							<select class='custom-select' id='proveedor' disabled>
							<option selected>No hay categorías en el sistema aún.</option>
							</select>
							";
						}
						else{
							echo "<select class='custom-select' id='tipo' name='tipo'>
							<option selected>Seleccione</option>
							";
							while ($columna = mysqli_fetch_array($resultado)){
								echo"
								<option value='".$columna["categoria"]."'>".$columna["categoria"]."</option>
								";
							}
							echo"</select>";
						}
						mysqli_close($conexion);
						?>
					</select>
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block; margin: -1% 0% !important;">Ingrese la referencia del producto<label class="rqrd">*</label></label>
					<input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Nombre del producto" onkeyup="ref();" onchange="ref();">
					<div id="resultref" style="margin-bottom: 1%;"></div>
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block; margin: -1% 0% !important;">Ingrese las unidades disponibles<label class="rqrd">*</label></label>
					<input type="text" min="0" class="form-control" id="cantidad" name="cantidad" placeholder="0">
				</div>
			</div>
			<div class="col-lg-6" style="float:left;">
				<div class="form-group">
					<label style="text-align: left; display: block; margin: 1% 0% !important;">Imagen</label>
					<input class="form-control" maxlength="150" type="file" name="imagen" id="imagen" data-validation-allowing="jpg, png, gif">
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block; margin: -1% 0% !important;">Ingrese el valor de inversión<label class="rqrd">*</label></label>
					<input type="text" class="form-control" id="valorI" name="valorI" placeholder="$0.00">
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block; margin: -1% 0% !important;">Ingrese el valor de venta<label class="rqrd">*</label></label>
					<input type="text" class="form-control" id="valorV" name="valorV" placeholder="$0.00">
				</div>
			</div>
			<div class="col-lg-12" style="float:left;">
				<div class="form-group">
					<label style="text-align: left; display: block; margin: 0% 0% !important;">Descripción del producto</label>
					<textarea type="text" class="form-control" id="descripcion" name="descripcion"></textarea>
				</div>
				<button type="button" class="btn btn-primary pull-left" onclick="Product('product');">Guardar</button>
			</div>
		</form>
	</div>
</div>
</div>
<!-- /Page Content -->

<!-- Footer -->
<?php
include('footer.php');
?>
<!-- /Footer -->
<?php
}
?>
