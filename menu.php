<body onload="op();">
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div>
      <a href="index.php" class="alog"><div class="logo"><p class="big"><strong>SWRAP</strong></p>
        <p class="little"><strong>Software de administración de productos.</strong></p>
      </div></a>
    </div>
    <div class="container">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="categoria.php">CATEGORÍA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">PROVEEDOR</a>
            <ul>
              <li><a href="proveedor.php">Nuevo Proveedor</a></li>
              <li><a href="administrar_proveedor.php">Administrar Proveedores</a></li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">PRODUCTO</a>
            <ul>
              <li><a href="producto.php">Nuevo Producto</a></li>
              <li><a href="Administrar_Productos.php">Administrar Productos</a></li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">CLIENTES</a>
            <ul>
              <li><a href="Nuevo_Cliente.php">Nuevo Cliente</a></li>
              <li><a href="Administrar_Clientes.php">Administrar Clientes</a></li>
            </ul>
          </li>
          <li class="nav-item" id="marg">
            <a class="nav-link" href="#">VENTAS</a>
            <ul>
              <li><a href="Nueva_Venta.php">Nueva Venta</a></li>
              <li><a href="Historial_Ventas.php">Historial de Venta</a></li>
            </ul>
          </li>
          <li class="nav-item ml-auto">
            <a class="navbar-brand" href="#"><img src="img/user.png" class="user"> <?php echo utf8_decode ($_SESSION['nombre']); ?></a>
            <ul class="lft">
              <li class="nav-item">
                <a class="nav-link" href='#' data-toggle='modal' data-target='#Admin'>Empresa</a>
                <a class="nav-link" href='#' data-toggle='modal' data-target='#Info'>Acerda de</a>
                <a class="nav-link" href="logout.php">Cerrar sesión</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div id="Admin" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 750px !important; max-width: 750px;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
          <label style="color: #FFF;">Información de Empresa</label>
          <button type="button" class="close" data-dismiss="modal" style="color: #FFF;">&times;</button>
        </div>
        <div class="form_amt" id="product">
          <form id="form_buss" method="post" enctype="multipart/form-data">
            <div style="width: 48%; float: left; margin: 1%;">
              <input size="10" type="text" class="form-control" id="id" name="id" style="display: none;">
              <div class="form-group">
                <label style="text-align: left; display: block; margin: -1% 0% !important;">Nombre completo de la Empresa<label class="rqrd">*</label></label>
                <input size="10" type="text" class="form-control" id="nempr" name="nempr" aria-describedby="emailHelp">
              </div>
              <div class="form-group">
                <label style="text-align: left; display: block; margin-bottom: -1%;">NIT</label>
                <input size="10" type="text" class="form-control" id="nit" name="nit" aria-describedby="emailHelp">
              </div>
              <div class="form-group">
                <label style="text-align: left; display: block; margin-bottom: -0.5%;">Dirección<label class="rqrd">*</label></label>
                <input size="10" type="text" class="form-control" id="drccn" name="drccn" aria-describedby="emailHelp">
              </div>
              <div class="form-group">
                <label style="text-align: left; display: block; margin-bottom: -2%;">Teléfono<label class="rqrd">*</label></label>
                <input size="10" type="text" class="form-control" id="tlfn" name="tlfn" aria-describedby="emailHelp">
              </div>
            </div>
            <div style="width: 48%; float: left; margin: 1%;">
              <div class="form-group">
                <label style="text-align: left; display: block; margin: 0% 0% !important;">Iva %</label>
                <input size="10" type="text" class="form-control" id="iva" name="iva" aria-describedby="emailHelp">
              </div>
              <div class="form-group">
                <label style="text-align: left; display: block; margin: -0.5% 0% !important;">Logo</label>
                <img class="iconForm" id="iconFormE"></img>
                <input size="10" type="file" class="form-control" id="log" name="log" aria-describedby="emailHelp" style="color: #FFF;" onchange="filetype();">
              </div>
              <div class="form-group">
                <label style="text-align: left; display: block; margin: 0% 0% !important;">Consecutivo de Factura</label>
                <input size="10" type="text" class="form-control" id="confct" name="confct" aria-describedby="emailHelp">
              </div>
            </div>
            <div style="width: 98%; float: left; margin-left: 1%;">
              <div class="form-group">
                <label style="text-align: left; display: block; margin: 0% 0% !important;">Pie de factura</label>
                <textarea size="10" class="form-control" id="pdftr" name="pdftr" aria-describedby="emailHelp"></textarea>
              </div>
            </div>
            <button id="updbss" type="button" class="btn btn-primary" onclick="SaveAdmon();">Guardar</button>
          </form>
        </div>
        <div id="resultbussin" style="margin-bottom: 10px; width: 80%; margin-left: 10%;"></div>
        <!--  -->
      </div>
    </div>
  </div>

  <div id="Info" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
          <label style="color: #FFF;">Acerca de SWRAP</label>
          <button type="button" class="close" data-dismiss="modal" style="color: #FFF;">&times;</button>
        </div>
        <div style="margin: 5%;">
          <p align="justify">
            <strong>SWRAP</strong> Se distribuye ùnicamente a puntos autorizados y cualquier intento de copia, rèplica, modificaciòn
            o uso malintencionado, se considera ilegal y se denunciarà ante las autoridades completentes de plagio y derechos de autor.</p>
            <p align="justify">Para adquirir de forma legal el software, se puede comunicar al nùmero: + (57) 323 364 4149.
            </p>
            <p align="center">2018 - Todos los derechos reservados.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- /Title Page-->
    <div class="row">
      <div class="col-md-3">
        <p class="leftTitle" id="top">
          <?php
          $where = $_SERVER['PHP_SELF'];
          if ($where == "/swrap/index.php"){
            echo "HOME.";
          }
          else{
            echo strtoupper(substr(str_replace("_"," ",$where), 7, -3));
          }
          ?>
        </p>
      </div>
      <div class="col-md-2" style="margin-left: 57%;">
        <p align="right"><a href="Nueva_Venta.php" class='btn btn-primary'>Nueva Venta</a></p>
      </div>
    </div>
    <!-- /Title Page-->
