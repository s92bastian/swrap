<?php
//Inicia validación de credenciales.
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: login.php");
  exit;
}

else{
  include("Conexion.php");
  $id = $_POST["id"];
  $empresa = $_POST["nempr"];
	$nit = $_POST["nit"];
	$direccion = $_POST["drccn"];
	$telefono = $_POST["tlfn"];
	$iva = $_POST["iva"];
	$pieFactura = $_POST["pdftr"];
  $consecutivoFactura = $_POST["confct"];
  $lg = mysqli_fetch_row(mysqli_query($conexion, "SELECT logo FROM empresa WHERE id = '".$id."'"));
  $imagen = $lg[0];
  $haveImg = 0;
  if ($_FILES["log"]["name"] != ""){
    $haveImg = 1;
    $nombre_temporal = $_FILES["log"]["tmp_name"];
    $imagen = $_FILES["log"]["name"];
    //Borrar imagen antigua
    if (file_exists('images/'.$lg[0])) {
      chmod('images', 0777 );
      unlink('images/'.$lg[0]);
    }
  }
  // Establecer consulta
  $consulta = "
  UPDATE empresa
  SET empresa = '".$empresa."',
  nit = '".$nit."',
  direccion = '".$direccion."',
  telefono = '".$telefono."',
  iva = ".$iva.",
  pieFactura = '".$pieFactura."',
  consecutivoFactura = ".$consecutivoFactura.",
  logo = '".$imagen."'
  WHERE id = ".$id."
  ";

  // Encapsular resultado
  mysqli_query($conexion, $consulta) or die ("Error grave! : ".mysqli_error($conexion)."Conacte al administrador del sistema.");
  if (mysqli_affected_rows($conexion) > 0){

    if ($haveImg == 1){
      if (move_uploaded_file($nombre_temporal, 'images/'.$imagen)){
        echo "
        <html>
        <body style='margin: 0; padding: 0;'>
        <a href='index.php'><img src='img/uok.png' style='width: 100% !important; height: 100% !important; margin: 0; padding: 0;'></a>
        </body>
        </html>
        ";
      }
      else{
        echo "
        <html>
        <body style='margin: 0; padding: 0;'>
        <a href='index.php'><img src='img/fail.png' style='width: 100% !important; height: 100% !important; margin: 0; padding: 0;'></a>
        </body>
        </html>
        ";
      }
    } else{
        echo "
        <html>
        <body style='margin: 0; padding: 0;'>
        <a href='index.php'><img src='img/uok.png' style='width: 100% !important; height: 100% !important; margin: 0; padding: 0;'></a>
        </body>
        </html>
        ";
      }

  } else {
      echo "
      <html>
      <body style='margin: 0; padding: 0;'>
      <a href='index.php'><img src='img/ufail.png' style='width: 100% !important; height: 100% !important; margin: 0; padding: 0;'></a>
      </body>
      </html>
      ";
    }
  mysqli_close($conexion);
}
?>
