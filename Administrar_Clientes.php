<?php
session_start();
if(!isset($_SESSION['usuario']))
{
	header("Location: login.php");
	exit;
}
else
{
	?>
	<!-- Header -->
	<?php
	include('head.php');
	if(!$_GET){
		header('Location:Administrar_Clientes.php?page=1');
	}
	?>
	<script src="js/functions.js"></script>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt_full">
		<div class="row">
			<div class="col-lg-12 text-left">
				<div class="form-inline">
					<div class="form-group mb-2" style="margin: 1% 1%;">
						<span><strong>Busqueda:</strong><img src="img/search.png" class="icon"></span>
					</div>
					<div class="form-group mx-sm-3 mb-2" style="margin: 0.5% 0%;">
						<input style="width: 500px;" type="text" class="form-control" id="search" placeholder="Ingresar un parámetro de busqueda..." onkeyup="searcCl()">
					</div>
					<div id="resultS" style="margin-left: 10px; margin-bottom: 2px;"></div>
				</div>
			</div>
			<div class="col-lg-12 text-center">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th scope="col">Nombre</th>
							<th scope="col">Apellidos</th>
							<th scope="col">Cédula</th>
							<th scope="col">Celular</th>
							<th scope="col">Correo</th>
							<th scope="col">Acciones</th>
						</tr>
					</thead>
					<tbody id="printTable">
						<?php
						if (isset($_POST["objeto"])){

						}
						else {

							$consulta = "SELECT * FROM cliente ORDER BY id ASC";
							include("Conexion.php");
							$resultado = mysqli_query($conexion, $consulta);
							$Tregistros = mysqli_num_rows($resultado);
							if(mysqli_num_rows($resultado) == 0){
								echo "<tr><td></td><td></td><td></td><td><p align='center'>No hay datos...</p></td><td></td><td></td></tr>";
							}
							else{
								$Article_x_Page = 10;
								$init = ($_GET['page']-1)*$Article_x_Page;
								$consulta2 = "SELECT * FROM cliente ORDER BY id ASC LIMIT ".$init.", ".$Article_x_Page."";
								$result = mysqli_query($conexion, $consulta2);
								$table = "cliente";
								$Tpages = ceil($Tregistros/$Article_x_Page);
								while ($columna = mysqli_fetch_array($result)){
									echo"
									<tr>
									<th scope='row'>".$columna['nombre']."</th>
									<td>".$columna['apellidos']."</td>
									<td>".$columna['cedula']."</td>
									<td>".$columna['celular']."</td>
									<td>".$columna['correo']."</td>
									<td><a href='#' data-toggle='modal' data-target='#UpdProduct' onclick='capdataucl(".chr(34).$columna['id'].chr(34).");'><img src='img/edit.png' class='icon' alt='Editar'></a> / <a href='#'  onclick='Delete(".chr(34).$columna["id"].chr(34).", ".chr(34).$table.chr(34).");'><img src='img/delete.png' class='icon' alt='Eliminar'></a></td>
									</tr>
									";
								}
							}
							mysqli_close($conexion);
							?>
						</tbody>
					</table>
				<?php } ?>
				<div class="container-fluid">

					<!-- Paginación -->
					<nav aria-label="Page navigation example" class="pagn">
						<ul class="pagination justify-content-center pag">
							<?php
							if($Tregistros > 10){
								$atribant = "";
								$atribsig = "";
								if($_GET['page'] == 1){
									$atribant = "disabled";
								}
								if ($_GET['page'] == $Tpages){
									$atribsig = "disabled";
								}
								echo "
								<li class='page-item ".$atribant."'><a class='page-link' href='Administrar_Productos.php?page=".($_GET['page']-1)."'>Anterior</a></li>
								";
								for($i = 1 ; $i <= $Tpages ; $i++) {
									$atribute = "";
									if ($_GET['page'] == $i){
										$atribute = "active";
									}
									echo "
									<li class='page-item ".$atribute."'><a class='page-link' href='Administrar_Clientes.php?page=".$i."'>".$i."</a></li>
									";

								}
								echo "
								<li class='page-item ".$atribsig."'><a class='page-link' href='Administrar_Clientes.php?page=".($_GET['page']+1)."'>Siguiente</a></li>
								";
							}
							?>
						</ul>
					</nav>
					<!-- /Paginación -->

				</div>
			</div>
		</div>


		<!-- Formulario de edición -->
		<div id="UpdProduct" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
						<label style="color: #FFF;">Editar Cliente</label>
						<button type="button" class="close" data-dismiss="modal" onclick="reload();" style="color: #FFF;">&times;</button>
					</div>
					<div style="width: 100%; padding:1%;" id="product">
						<div class="form_amt" id="client">
							<strong>Información del Cliente</strong>
							<div id="resultnvclnt" style="margin-top: 8px;"></div>
							<input id="PF" name="PF" type="hidden" value="A">
							<input type="text" id="idcl" style="display: none;">
							<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
							<div class="form-group">
								<label style="text-align: left; display: block; margin: 0% 0% !important;">Nombres<label class="rqrd">*</label></label>
								<input type="text" class="form-control" id="nombre" aria-describedby="emailHelp">
							</div>
							<div class="form-group">
								<label style="text-align: left; display: block; margin: -0% 0% !important;">Apellidos<label class="rqrd">*</label></label>
								<input type="text" class="form-control" id="apellidos" aria-describedby="emailHelp">
							</div>
							<div class="form-group">
								<label style="text-align: left; display: block;">Número de cédula</label>
								<input type="text" class="form-control" id="cedulaM" aria-describedby="emailHelp">
							</div>
							<div class="form-group">
								<label style="text-align: left; display: block; margin: -0% 0% !important;">Celular<label class="rqrd">*</label></label>
								<input type="text" class="form-control" id="celular" aria-describedby="emailHelp">
							</div>
							<div class="form-group">
								<label style="text-align: left; display: block;">Correo electrónico</label>
								<input type="text" class="form-control" id="correo" aria-describedby="emailHelp">
							</div>
							<button id="updcl" type="submit" class="btn btn-primary" onclick="UpdPerson();">Editar</button>
					</div>
					<div class="col-lg-12" style="float:left; margin-bottom: 1%;"><div id="resultnvclnt"></div></div>
				</div>
			</div>
		</div>

		<!-- /Formulario de edición -->

	</div>
	<!-- /Page Content -->

	<!-- Footer -->
	<?php
	include('footer.php');
	?>
	<!-- /Footer -->
	<?php
}
?>
