<?php
session_start();
if(!isset($_SESSION['usuario']))
{
	header("Location: login.php");
	exit;
}
else
{
	?>
	<!-- Header -->
	<?php
	include('head.php');
	?>
	<script src="js/functions.js"></script>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="form_amt" id="person">
					<div id="resultprvr"></div>
					<strong>Información del Proveedor</strong>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: -1% 0% !important;">Nombre del proveedor<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="nombrep" placeholder="Ej: Samsung">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: -1% 0% !important;">Nombre del contacto<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="nombrec" placeholder="Jhon..">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: 0.2% 0% !important;">Teléfono de contácto</label>
						<input type="text" class="form-control" id="telefono" placeholder="+(57)">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: -1% 0% !important;">Celular de contácto<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="celular" placeholder="+(57)">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: 0.2% 0% !important;">Correo electrónico<label class="rqrd">*</label></label>
						<input type="email" class="form-control" id="correo" placeholder="correo@electronico.com">
					</div>
					<div class="form-group">
						<label style="text-align: left; display: block; margin: 0.2% 0% !important;">Dirección del proveedor</label>
						<input type="text" class="form-control" id="direccion" placeholder="Dirección">
					</div>
					<button type="submit" class="btn btn-primary pull-left" onclick="Provee();">Guardar</button>

				</div>
			</div>
		</div>
	</div>
	<!-- /Page Content -->

	<!-- Footer -->
	<?php
	include('footer.php');
	?>
	<!-- /Footer -->
	<?php
}
?>
