<?php
session_start();
if(!isset($_SESSION['usuario']))
{
	header("Location: login.php");
	exit;
}
else
{
	?>
	<!-- Header -->
	<?php
	include('head.php');
	?>
	<script src="js/functions.js"></script>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt">
		<div class="row">
			<div class="col-lg-6 text-center">
				<strong style="margin-bottom: 10px; text-align: left !important;">AGREGAR CATEGORÍA</strong>
				<div class="form_amt" id="person">
					<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->

					<div class="form-group">
						<label style="text-align: left; display: block; margin: -2% 0% !important;">Ingrese el nombre de la categoría<label class="rqrd">*</label></label>
						<input type="text" class="form-control" id="categ" aria-describedby="emailHelp" placeholder="Nombre de la categoría">
					</div>
					<button type="submit" class="btn btn-primary pull-left" onclick="Categ();">Guardar</button>
					<div id="resultctg" style="margin-top:1%;"></div>
				</div>
			</div>
			<div class="col-lg-6 text-center">
				<strong style="margin-bottom: 10px; text-align: left !important;">LISTADO DE CATEGORÍAS</strong>

				<div class="row">
					<div class="col-lg-12 text-center">

						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th scope="col">Código</th>
									<th scope="col">Categoría</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								include("Conexion.php");
								$consulta = "
								SELECT *
								FROM categoria
								";
								$table = "categoria";
								$resultado = mysqli_query($conexion, $consulta);
								if(mysqli_num_rows($resultado) == 0){
									echo "<tr><td></td><td><p align='center'>No hay datos...</p></td><td></td></tr>";
								}
								else{
									while ($columna = mysqli_fetch_array($resultado)){
										echo"
										<tr>
										<td><strong>".$columna['id']."</strong></td>
										<td>".$columna['categoria']."</td>
										<td><a href='#' data-toggle='modal' data-target='#UpdCateg' onclick='capdataupdcateg(".chr(34).$columna['id'].chr(34).");'><img src='img/edit.png' class='icon' alt='Editar'></a> / <a href='#'  onclick='Delete(".chr(34).$columna["id"].chr(34).", ".chr(34).$table.chr(34).");'><img src='img/delete.png' class='icon' alt='Eliminar'></a></td>
										</tr>
										";
									}
								}
								mysqli_close($conexion);
								?>
							</tbody>
						</table>
					</div>
				</div>


				<!-- Formulario de edición -->
				<div id="UpdCateg" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
								<label style="color: #FFF;">Editar Categoría</label>
								<button type="button" class="close" data-dismiss="modal" onclick="reload();" style="color: #FFF;">&times;</button>
							</div>
							<div class="form_amt" id="product">
								<input type="text" id="idc" style="display: none;">
								<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -2% 0% !important;">Nombre de la categoría<label class="rqrd">*</label></label>
									<input type="text" class="form-control" id="catego" aria-describedby="emailHelp" placeholder="Nombre de la categoría">
								</div>
								<button id="updpr" type="submit" class="btn btn-primary" onclick="UpdateCateg();">Editar</button>
								<div id="result2" style="margin-top: 8px;">
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- /Formulario de edición -->

			</div>
		</div>
	</div>
	<!-- /Page Content -->

	<!-- Footer -->
	<?php
	include('footer.php');
	?>
	<!-- /Footer -->
	<?php
}
?>
