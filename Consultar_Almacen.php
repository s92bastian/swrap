<?php 
	session_start(); 
  	if(!isset($_SESSION['usuario']))
	{
    header("Location: login.php");
    exit;
	}
  else 
  { 
?> 
		<!-- Header -->
		<?php
		include('head.php');
		?>
		<script src="js/functions.js"></script>
		<!-- Header -->

		<!-- Menu -->
		<?php
		include('menu.php');
		?>
		<!-- /Menu -->
	
		<!-- Page Content -->
			<div class="containeramt_full">
				<div class="row">
					<div class="col-lg-12 text-center">
						<table class="table table-striped">
						  <thead class="thead-dark">
							<tr>
							  <th scope="col">Código</th>
							  <th scope="col">Producto</th>
							  <th scope="col">Unidades disponibles</th>
							  <th scope="col">Valor de Venta Inversionista</th>
							  <th scope="col">Valor de Venta Revendedor</th>
							  <th scope="col">Valor de Venta Final</th>
							  <th scope="col">Acciones</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							include("Conexion.php");
							$consulta = "SELECT id_producto as id_producto, cantidad as cantidad, venta_i as venta_i, venta_r as venta_r, valor_venta as valor_venta, nombre
										 FROM almacen
										 JOIN producto 
										 ON almacen.id_producto = producto.id
										";
								$resultado = mysqli_query($conexion, $consulta);
								if(mysqli_num_rows($resultado) == 0){
									echo "<tr><td></td><td></td><td></td><td><p align='center'>No hay datos...</p></td></tr>";
								}
								else{
									while ($columna = mysqli_fetch_array($resultado)){
										echo"
											<tr>
											  <td><strong>".$columna['id_producto']."</strong></td>
											  <td>".$columna['nombre']."</td>
											  <td>".$columna['cantidad']."</td>
											  <td>$ ".number_format($columna['venta_i'], 0, '', '.')."</td>
											  <td>$ ".number_format($columna['venta_r'], 0, '', '.')."</td>
											  <td>$ ".number_format($columna['valor_venta'], 0, '', '.')."</td>
											  <td><a href='#' data-toggle='modal' data-target='#UpdProduct' onclick='capdataupd(".chr(34).$columna['id_producto'].chr(34).");'><img src='img/edit.png' class='icon' alt='Editar'></a> / <a href='#'  onclick='Delete(".chr(34).$columna["id_producto"].chr(34).");'><img src='img/delete.png' class='icon' alr='Eliminar'></a></td>
											</tr>
										";
									}
								}
								mysqli_close($conexion);
							?>
						  </tbody>
						</table>
					</div>
				</div>
				
				
				<!-- Formulario de edición -->
				<div id="UpdProduct" class="modal fade" role="dialog">  
				  <div class="modal-dialog">  
			   <!-- Modal content-->  
					   <div class="modal-content">  
							<div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
								<label style="color: #FFF;">Editar Producto</label>
								<button type="button" class="close" data-dismiss="modal" onclick="reload();" style="color: #FFF;">&times;</button>  
							</div>  
							<div class="form_amt" id="product">
							<input type="text" id="id" style="display: none;">
							<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -2% 0% !important;">Nombre del producto<label class="rqrd">*</label></label>
									<input type="text" class="form-control" id="nombre" aria-describedby="emailHelp" placeholder="Nombre del producto">
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -2% 0% !important;">Valor de inversión<label class="rqrd">*</label></label>
									<input type="text" class="form-control" id="valorI" placeholder="$0.00">
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: -2% 0% !important;">Valor de venta<label class="rqrd">*</label></label>
									<input type="text" class="form-control" id="valorV" placeholder="$0.00">
								</div>
								<div class="form-group">
									<label style="text-align: left; display: block; margin: 0% 0% !important;">Descripción del producto</label>
									<textarea type="text" class="form-control" id="descripcion"></textarea>
								</div>
								<button id="updpr" type="submit" class="btn btn-primary" onclick="UpdateProduct();">Editar</button>
								<div id="result">
								</div>
						</div>
					   </div>  
				  </div>  
				</div> 
				
				<!-- /Formulario de edición -->
				
			</div>
		<!-- /Page Content -->
	
		<!-- Footer -->
		<?php
		include('footer.php');
		?>
		<!-- /Footer -->
<?php
  }
?>