<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;" charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="img/favicon.png" />
	<title>SWRAP</title>

	<!-- Bootstrap core CSS -->
	<script src="js/functions.js"></script>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-grid.css" rel="stylesheet" type="text/css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link rel='stylesheet' href='bower_components/glyphicons-only-bootstrap/css/bootstrap.min.css' />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style>
	body {
		padding-top: 54px;
	}
	@media (min-width: 992px) {
		body {
			padding-top: 56px;
		}
	}

	</style>
	<head>
