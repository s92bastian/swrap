<?php 
	session_start(); 
  	if(!isset($_SESSION['usuario']))
	{
    header("Location: login.php");
    exit;
	}
  else 
  { 
?> 
		<!-- Header -->
		<?php
		include('head.php');
		?>
		<!-- Header -->

		<!-- Menu -->
		<?php
		include('menu.php');
		?>
		<!-- /Menu -->
	
		<!-- Page Content -->
			<div class="containeramt">
				<div class="row">
				
					<div class="col-lg-6 text-center">
						<img src="img/almacen.png" class="middleimg">
						<p class="txt">
						<strong>Almacenamiento de productos.</strong><br>
						Los productos almacenados, se registran una única vez. En este apartado se pobla el sistema con la cantidad y disponibilidad de los mismos de forma que se pueda tener control
						del stock y las existencias en tiempo real.
						</p>
						<br>
						<p class="txt"><strong>SWRAP</strong></p>
					</div>
					
					<div class="col-lg-6 text-center">
						<div class="form_amt" id="person">
							<?php
								include("Conexion.php");
								function select($parametro){
								return "
									<div class='form-group'>
										<label style='text-align: left; display: block; margin: -2% 0% !important;'>Seleccione el producto<label class='rqrd'>*</label></label>
										<select class='custom-select' id='id_producto'  disabled>
										<option selected>".$parametro."</option>
										</select>
									</div>
									<div class='form-group'>
										<label style='text-align: left; display: block; margin: -2% 0% !important;'>Ingrese la cantidad de unidades disponibles del producto.<label class='rqrd'>*</label></label>
										<input type='text' class='form-control' id='cantidad' placeholder='0'  disabled>
									</div>
									<button id='almacbut' type='submit' class='btn btn-primary' onclick='Almac();'  disabled>Guardar</button>
									<div id='result'></div>
								";
								}
								$consulta0 = "SELECT * FROM producto";
								$res = mysqli_query($conexion, $consulta0) or die ( "Ocurrio un error. Contacte al administrador del sistema");
								if(mysqli_num_rows($res) == 0){
									$parametro = "Aún no hay productos registrados en el sistema";
									echo select($parametro);
								}
								else{
									$consultauno = "SELECT id, nombre FROM producto where Not id In(select id_producto from almacen)";
									$resultado = mysqli_query($conexion, $consultauno) or die ( "Ocurrio un error. Contacte al administrador del sistema");
									if(mysqli_num_rows($resultado) == 0){
										$parametro = "Ya se han registrado todos los productos.";
										echo select($parametro);
									}
									else{
										echo"
											<div class='form-group'>
												<label style='text-align: left; display: block; margin: -2% 0% !important;'>Seleccione el producto<label class='rqrd'>*</label></label>
												<select class='custom-select' id='id_producto'>
													<option selected>Seleccione</option>
										";
										while ($columna = mysqli_fetch_array($resultado)){
											echo"
												<option value='".$columna["id"]."'>".$columna["nombre"]."</option>
											";
										}
											echo"
												</select>
												</div>
												<div class='form-group'>
													<label style='text-align: left; display: block; margin: -2% 0% !important;'>Ingrese la cantidad de unidades disponibles del producto.<label class='rqrd'>*</label></label>
													<input type='text' class='form-control' id='cantidad' placeholder='0'>
												</div>
												<button id='almacbut' type='submit' class='btn btn-primary' onclick='Almac();'>Guardar</button>
												<div id='result'></div>
											";	
									}
								}
							?>
						</div>
					</div>
					
				</div>
			</div>
			
		<!-- /Page Content -->
	
		<!-- Footer -->
		<?php
		include('footer.php');
		?>
		<!-- /Footer -->
<?php
  }
?>