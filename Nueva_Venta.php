<?php
session_start();
if(!isset($_SESSION['usuario'])) {
	header("Location: login.php");
	exit;
} else {
	?>
	<!-- Header -->
	<?php
	include('head.php');
	?>
	<script src="js/functions.js"></script>
	<!-- Header -->

	<!-- Menu -->
	<?php
	include('menu.php');
	?>
	<!-- /Menu -->

	<!-- Page Content -->
	<div class="containeramt">
		<strong style="margin-left: 1%;">FACTURA</strong>
		<div class="row">
			<div class="col-lg-4">
				<div class="form_amt" id="person">
					<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					<div class="form-group">
						<input id="idTransact" name="idTransact" type="hidden" value="0">
						<label style="text-align: left; display: block;">Nombre del cliente<label class="rqrd">*</label></label>
						<table style="width: 100%;"><tr><td><input type="text" class="form-control" id="cliente" aria-describedby="emailHelp" placeholder="Nombre del cliente" onkeyup="searcPerson()"><td><a id='clienteA' href="#" data-toggle="modal" data-target="#Person"><img src="img/more.png" class="more"></a></td></tr></table>
						<div id="resultS" style="width: 80%; margin-top: 5px; z-index: 9999;"></div>
						<div id="printClient"></div>
					</div>
					<div class="form-group">
						<label style="display: block;">N. Cédula</label>
						<input type="text" style="width: 86%;" class="form-control" id="cedula" placeholder="Cédula" onkeyup="searcCedul()">
						<div id="resultCed" style="width: 80%; margin-top: 5px; z-index: 9999;"></div>
						<div id="printC"></div>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="form_amt" id="person">
					<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					<div class="form-group">
						<label style="text-align: left; display: block;">Producto<label class="rqrd">*</label></label>
						<?php
						include("Conexion.php");
						$consulta = "SELECT * FROM producto";
						$resultado = mysqli_query($conexion, $consulta);
						if(mysqli_num_rows($resultado) == 0){
							echo"
							<select onchange='calc_purchase()' style='width: 86%;' class='custom-select' id='producto' disabled>
							<option selected>No hay productos en el sistema aún.</option>
							</select>
							";
						}
						else{
							echo "<select onchange='calc_purchase()' style='width: 86%;' class='custom-select' id='producto'>
							<option value='Seleccione' selected>Seleccione</option>
							";
							while ($columna = mysqli_fetch_array($resultado)){
								echo"
								<option value='".$columna["id"]."'>".$columna["nombre"]."</option>
								";
							}
							echo"</select>";
						}

						?>
					</select>
				</div>
				<div class="form-group">
					<label style="display: block;">% Descuento sobre la ganancia</label>
					<input onkeyup="calc_purchase()" type="text" style="width: 86%;" class="form-control" id="descuento" placeholder="Ej: 30%" value="0">
				</div>
			</div>
		</div>

		<div class="col-lg-4">
			<div class="form_amt" id="person">
				<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
				<div class="form-group">
					<label style="text-align: left; display: block;"># Unidades<label class="rqrd">*</label></label>
					<input onkeyup="calc_purchase()" type="text" style='width: 86%;' class="form-control" id="unidades" aria-describedby="emailHelp" value="1">
				</div>
				<div class="form-group">
					<label style="display: block;">Precio final</label>
					<input type='text' style='width: 86%;' class='form-control' id='precio' disabled>
				</div>
			</div>
			<button type="submit" class="btn btn-primary" style="float: right; margin-right: 5px;" onclick="NewFact();">Agregar a la factura</button>
		</div>

		<div class="col-lg-12 text-center">

			<div id="resultP" style="margin-top: 10px;">
			</div>

		</div>
	</div>
	<!-- <hr class="style3"> -->
	<hr>
</div>
<div class="containeramt text-center" id="tmp_sale">

</div>

<!-- Formulario de edición -->
<div id="Person" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding: 0.5rem !important; margin-bottom: 15px; background-color: #343a40;">
				<label style="color: #FFF;">Nuevo Cliente</label>
				<button type="button" class="close" data-dismiss="modal" style="color: #FFF;">&times;</button>
			</div>
			<div class="form_amt" id="product">
				<input type="text" id="idc" style="display: none;">
				<input id="PF" name="PF" type="hidden" value="B">
				<!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
				<div class="form-group">
					<input id="referenciaCliente" name="referenciaCliente" type="hidden" value="1">
					<label style="text-align: left; display: block; margin: -2% 0% !important;">Nombres<label class="rqrd">*</label></label>
					<input type="text" class="form-control" id="nombre" aria-describedby="emailHelp">
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block; margin: -2% 0% !important;">Apellidos<label class="rqrd">*</label></label>
					<input type="text" class="form-control" id="apellidos" aria-describedby="emailHelp">
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block;">Número de cédula</label>
					<input type="text" class="form-control" id="cedulaM" aria-describedby="emailHelp" onkeyup="ced()" onchange="ced()">
					<div id="resultced" style="margin-bottom: 1%;"></div>
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block; margin: -2% 0% !important;">Celular<label class="rqrd">*</label></label>
					<input type="text" class="form-control" id="celular" aria-describedby="emailHelp">
				</div>
				<div class="form-group">
					<label style="text-align: left; display: block;">Correo electrónico<label class="rqrd">*</label></label>
					<input type="text" class="form-control" id="correo" aria-describedby="emailHelp">
				</div>
				<button id="updpr" type="submit" class="btn btn-primary" onclick="SavePerson();">Guardar</button>
				<div id="resultnvclnt" style="margin-top: 8px;"></div>
			</div>
		</div>
	</div>
</div>

<!-- /Formulario de edición -->
<!-- /Page Content -->

<!-- Footer -->
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(e) {

	document.addEventListener("click", function(e) {
		var container = $("#printClient");
		var container2 = $("#resultCed");

		if (!container.is(e.target) && container.has(e.target).length === 0) {
			$('#printClient').css({'display':'none'});
		}

		if (!container2.is(e.target) && container2.has(e.target).length === 0) {
			$('#resultCed').css({'display':'none'});
		}
	});

});
</script>
<?php
include('footer.php');
?>
<!-- /Footer -->
<?php
}
?>
