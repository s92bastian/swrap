<?php
//Inicia validación de credenciales.
session_start();
if(!isset($_SESSION['usuario'])){
	header("Location: login.php");
	exit;
}
else{
	date_default_timezone_set('America/Bogota');
	$objeto = json_decode($_POST["objeto"], true);
	$id_transact = $objeto["id_transact"];
	$cliente = $objeto["cliente"];
	$cedula = $objeto["cedula"];
	$producto = $objeto["producto"];
	$descuento = $objeto["descuento"];
	$unidades = $objeto["unidades"];
	$precio = $objeto["precio"];
	$fecha = date("Y-m-d");
	$table = "tmp_sale";
	$respons="";
	include("Conexion.php");
	// Validar cantidad disponible de productos.
	$val = "
				SELECT cantidad
				FROM producto
				WHERE id LIKE '".$producto."'
	";
	$Rval = mysqli_query($conexion, $val);
	$Rrow = mysqli_fetch_array($Rval);
	// Validar cantidad de productos marcados como vendidos
	$val2 = "
				SELECT COUNT(*) as cantidad
				FROM tmp_sale
				WHERE final_purchase = 0
				AND id_producto = ".$producto."
	";
	$Rval2 = mysqli_query($conexion, $val2);
	$Rrow2 = mysqli_fetch_array($Rval2);
	// Comprobar disponibilidad
	if(($Rrow2['cantidad']+1) <= $Rrow['cantidad']){
	// Establecer consulta
	$consulta = "
	INSERT INTO tmp_sale
	(id_transact, cedula, nombre_c, id_producto, descuento, unidades, precioF, fecha)
	VALUES (".$id_transact.", '".$cedula."', '".$cliente."', '".$producto."', ".$descuento.", ".$unidades.", ".$precio.", '".$fecha."')
	";

	// Encapsular resultado
	if (mysqli_query($conexion, $consulta) or die (mysqli_error($conexion).$consulta)){

		mysqli_close($conexion);
		$consult = "
		SELECT ts.id, ts.id_transact as id_transact, ts.unidades as unidades, pr.nombre as nombre, ts.precioF as precio
		FROM tmp_sale ts JOIN producto pr ON pr.id = ts.id_producto
		AND ts.id_transact LIKE '".$id_transact."'
		";
		include("Conexion.php");

		if ($result = mysqli_query($conexion, $consult) or die ("Ocurrio un error. Contacte al administrador del sistema")){
			$respons = $respons."
			<form id='tmp_saleF' action='printer.php' method='POST' target='_blank'>
			<strong style='margin-left: 1%;'>DETALLE DE FACTURA</strong>
			<input type='hidden' id='id_transact' name='id_transact' value='".$id_transact."'>
			<table class='table'>
			<tr>
			<th>Unidades</th>
			<th>Producto</th>
			<th>Valor $</th>
			<th>Remover</th>
			</tr>
			";
			while($row = mysqli_fetch_array($result)){
				$respons = $respons."<tr id='".$row["id"]."' class='rmv'>
				<td>".$row['unidades']."</td><td>".$row['nombre']."</td><td>".$row['precio']."</td><td><a href='#' onclick='Delete(".chr(34).$row["id"].chr(34).", ".chr(34).$table.chr(34).")'><img src='img/delete.png' class='icon' alt='Eliminar'></a></td>
				</tr>";
			}
			$respons = $respons."</table>
			<button style='margin: 10px;' id='factur' type='submit' class='btn btn-primary' onclick='succesON()'>Generar Factura</button>
			</form>
			";

		}
		echo $respons;
	}
	else {
		echo false;
	}
}
else {
	echo 1053;
}
	mysqli_close($conexion);

}
?>
