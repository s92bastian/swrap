

/*
* [SWRAP]
*
* Sofware de Administración de productos.
*
*/


* Para instalar el programa es necesario:

1. Instalar Xampp u otro servidor local.

2. Instalar Git Bash

3. Dirigirse a la carpeta 'htdocs' del servidor local

4. Hacer click derecho y seleccionar la opción 'Git Bash Here'

5. Ejecutar el comando 'git clone https://gitlab.com/s92bastian/swrap.git'

6. Crear la base de datos 'swrap' en el motor MySql o en PhpMyAmdin

7. Restablecer copia de la base de datos almacenada en la carpeta /BD/swrap.sql, en MySql o en PhpMyAdmin.
