<!DOCTYPE html>
<html lang="en">
<link rel="icon" type="image/png" href="img/favicon.png" />
<title>SWRAP</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/functions.js"></script>
<link href="css/login.css" rel="stylesheet" type="text/css">

<!------ Enlaces a css, jquery y javascript ---------->
<body id="login">
  <div class="login">
    <div class="logo"><p class="big"><strong>SWRAP</strong></p>
      <p class="little"><strong>Software de administración de productos.</strong></p>
    </div>
    <!--  method="post" action="proc_log.php" -->
    <div id="formulario">
      <input type="text" id="usuario" name="usuario" placeholder="Usuario" required="required" />
      <input type="password" id="contrasena" name="contrasena" placeholder="Contraseña" required="required" />
      <button id="enviar" class="btn btn-primary btn-block btn-large" onclick="login();">Ingresar</button>
      <div id="resultlgn"></div>
    </div>
    <br>
    <!-- login bootsnipp -->

    <script type="text/javascript">
    (adsbygoogle = window.adsbygoogle || []).push({});
  </script>
</div>
</body>
</html>
